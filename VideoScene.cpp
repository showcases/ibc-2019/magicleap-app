#include <ml_logging.h>

#include "VideoScene.h"
#include "glsl_constants.h"

#define SINK_CAPS \
    "video/x-raw(" GST_CAPS_FEATURE_MEMORY_GL_MEMORY "), "              \
    "format = (string) RGBA, "                                          \
    "width = " GST_VIDEO_SIZE_RANGE ", "                                \
    "height = " GST_VIDEO_SIZE_RANGE ", "                               \
    "framerate = " GST_VIDEO_FPS_RANGE ", "                             \
    "texture-target = (string) { 2D, external-oes } "                   \

using namespace lumin;
using namespace lumin::ui;

VideoScene::VideoScene(BaseApp &app, const std::string &uri,
    uint32_t width, uint32_t height, bool stereoscopic, float widthFactor)
  : mUri(uri)
  , mApp(app)
  , mWidth(width)
  , mHeight(height)
  , mStereoscopic(stereoscopic)
  , mWidthFactor(widthFactor)
{
}

void VideoScene::onAttachPrism(Prism* prism)
{
  auto topLayout = UiLinearLayout::Create(prism);
  topLayout->setOrientation(Orientation::kVertical);
  topLayout->setAlignment(Alignment::CENTER_CENTER);
  topLayout->setDefaultItemAlignment(Alignment::CENTER_CENTER);
  getRoot()->addChild(topLayout);

  auto planarID = prism->createPlanarEGLResourceId(mWidth, mHeight);
  mPlanar = static_cast<PlanarResource*>(prism->getResource(planarID));
  mQuad = prism->createQuadNode(planarID);
  mQuad->setBackFaceCulls(false);
  mQuad->setViewMode(mStereoscopic ? ViewMode::kLeftRight : ViewMode::kFullArea);
  resize();

  auto videoRect = UiRectLayout::Create(prism);
  videoRect->setAlignment(Alignment::TOP_CENTER);
  videoRect->setContent(mQuad);
  videoRect->onActivateSub([this](const UiEventData& data) {
    ML_LOG(Info, "play/pause");
    mPlaying ? pause() : resume();
  });
  topLayout->addItem(videoRect);

  auto layoutWidth = prism->getSize().x;
  mSlider = UiSlider::Create(prism, layoutWidth * .7f);
  mSlider->onActivateSub([this](const UiEventData& data) {
    if (mPlaying) {
      pause();
    } else {
      seekTo(mSlider->getValue());
      resume();
    }
  });
  topLayout->addItem(mSlider, {.05f, .05f, .05f, .05f});


  mEGLDisplay = eglGetDisplay(EGL_DEFAULT_DISPLAY);
  mEGLContext = mPlanar->getEGLContext();
  mEGLSurface = mPlanar->getEGLSurface();

  mRunning = true;
  mThread = std::thread(&VideoScene::threadLoop, this);
}

void VideoScene::onDetachPrism(Prism* prism)
{
  mRunning = false;
  gst_element_set_state(mPlayBin, GST_STATE_NULL);
  mThread.join();

  g_clear_object(&mPlayBin);
  g_clear_object(&mDisplay);
  g_clear_object(&mContext);
  g_clear_object(&mOtherContext);

  // silently fails if those objects already = 0
  glDeleteShader(mVertexShaderId);
  glDeleteShader(mFragmentShaderId);
  glDeleteProgram(mProgramId);
}

void VideoScene::onUpdate(float delta)
{
  mLastUpdate += delta;
  if (mPlaying && mLastUpdate >= 1.0f) {
    gint64 position, duration;
    gst_element_query_position(mPlayBin, GST_FORMAT_TIME, &position);
    gst_element_query_duration(mPlayBin, GST_FORMAT_TIME, &duration);
    mSlider->setValue((float)position / duration);
    mLastUpdate = 0.0f;
  }
}

void VideoScene::pause()
{
  gst_element_set_state(mPlayBin, GST_STATE_PAUSED);
  mPlaying = false;
}

void VideoScene::resume()
{
  gst_element_set_state(mPlayBin, GST_STATE_PLAYING);
  mPlaying = true;
}

GstBusSyncReply VideoScene::busSyncHandlerCb(GstBus *bus, GstMessage *msg,
    gpointer user_data)
{
  VideoScene *self = static_cast<VideoScene*>(user_data);

  /* Do not let GstGL retrieve the display handle on its own
   * because then it believes it owns it and calls eglTerminate()
   * when disposed */
  if (GST_MESSAGE_TYPE(msg) == GST_MESSAGE_NEED_CONTEXT)
    {
      const gchar *type;
      gst_message_parse_context_type(msg, &type);
      if (g_str_equal(type, GST_GL_DISPLAY_CONTEXT_TYPE)) {
        GstContext *context = gst_context_new(GST_GL_DISPLAY_CONTEXT_TYPE, TRUE);
        gst_context_set_gl_display(context, self->mDisplay);
        gst_element_set_context(GST_ELEMENT(msg->src), context);
        gst_context_unref(context);
      } else if (g_str_equal(type, "gst.gl.app_context")) {
        GstContext *app_context = gst_context_new("gst.gl.app_context", TRUE);
        GstStructure *s = gst_context_writable_structure(app_context);
        gst_structure_set(s,
            "context", GST_TYPE_GL_CONTEXT, self->mOtherContext,
            NULL);
        gst_element_set_context(GST_ELEMENT(msg->src), app_context);
        gst_context_unref(app_context);
      }
    }
  else if (GST_MESSAGE_TYPE(msg) == GST_MESSAGE_ELEMENT)
    {
      if (gst_message_has_name (msg, "gst.mlaudiosink.need-app")) {
        g_object_set(G_OBJECT(msg->src), "app", &self->mApp, NULL);
      } else if (gst_message_has_name (msg, "gst.mlaudiosink.need-audio-node")) {
        /* AudioNode creation must be done here in a sync handler. */
        self->mAudioNode = self->getPrism()->createAudioNode();
        self->mAudioNode->setSpatialSoundEnable (true);
        self->mQuad->addChild(self->mAudioNode);
        self->resetAudioPosition();
        g_object_set(G_OBJECT(msg->src), "audio-node", self->mAudioNode, NULL);
      }
    }

  return GST_BUS_PASS;
}

void VideoScene::createPipeline()
{
  /* Wrap application gl context */
  GstGLPlatform gl_platform = GST_GL_PLATFORM_EGL;
  guintptr gl_handle = gst_gl_context_get_current_gl_context(gl_platform);
  GstGLAPI gl_api = gst_gl_context_get_current_gl_api(gl_platform, NULL, NULL);
  mDisplay = (GstGLDisplay *)g_object_ref_sink(gst_gl_display_new());
  mOtherContext = (GstGLContext *)g_object_ref_sink(gst_gl_context_new_wrapped(
      mDisplay, gl_handle, gl_platform, gl_api));

  mAppSink = gst_element_factory_make("appsink", NULL);
  g_object_set(mAppSink, "caps", gst_caps_from_string(SINK_CAPS), NULL);

  GstElement *glsinkbin = gst_element_factory_make("glsinkbin", NULL);
  g_object_set(glsinkbin, "sink", mAppSink, NULL);

  mPlayBin = gst_element_factory_make("playbin", NULL);
  g_object_set(mPlayBin,
      "video-sink", glsinkbin,
      "uri", mUri.c_str(),
      NULL);

  GstBus *bus = gst_element_get_bus(mPlayBin);
  gst_bus_set_sync_handler(bus, busSyncHandlerCb, this, NULL);
  g_object_unref(bus);

  gst_element_set_state(mPlayBin, GST_STATE_PLAYING);
  mPlaying = true;
}

void VideoScene::createGLTextureAndProgram()
{
  // Create shader objects
  mVertexShaderId = glCreateShader(GL_VERTEX_SHADER);
  mFragmentShaderId = glCreateShader(GL_FRAGMENT_SHADER);
  if (!mVertexShaderId || !mFragmentShaderId) {
    ML_LOG(Fatal, "Could not create shader objects !\n");
  }

  // Compile vertex and fragments shaders and program
  GLint compiled = 0;
  glShaderSource(mVertexShaderId, 1, &vertex_shader_source, NULL);
  glCompileShader(mVertexShaderId);

  glGetShaderiv(mVertexShaderId, GL_COMPILE_STATUS, &compiled);
  if (!compiled) {
    glDeleteShader(mVertexShaderId);
    ML_LOG(Fatal, "Vertex Shader did not compile !\n");
  }

  const char *fragment_shader_source = mTextureTarget == GL_TEXTURE_EXTERNAL_OES ?
      fragment_shader_source_oes : fragment_shader_source_2d;

  compiled = 0;
  glShaderSource(mFragmentShaderId, 1, &fragment_shader_source, NULL);
  glCompileShader(mFragmentShaderId);

  glGetShaderiv(mFragmentShaderId, GL_COMPILE_STATUS, &compiled);
  if (!compiled) {
    glDeleteShader(mFragmentShaderId);
    ML_LOG(Fatal, "Fragment Shader did not compile !\n");
  }

  mProgramId = glCreateProgram();
  if (!mProgramId) {
    glDeleteShader(mVertexShaderId);
    glDeleteShader(mFragmentShaderId);
    ML_LOG(Fatal, "Could not create GLSL program !\n");
  }

  glAttachShader(mProgramId, mVertexShaderId);
  glAttachShader(mProgramId, mFragmentShaderId);

  // Bind vPosition to attribute 0
  glBindAttribLocation(mProgramId, 0, "a_position");
  glBindAttribLocation(mProgramId, 1, "a_texcoord");

  GLint linked = 0;
  glLinkProgram(mProgramId);

  glGetProgramiv(mProgramId, GL_LINK_STATUS, &linked);
  if (!linked) {
    ML_LOG(Fatal, "GLSL Program did not link !\n");
  }
}

void VideoScene::renderGLTexture()
{
  glUseProgram(mProgramId);

  GLint positionLoc = glGetAttribLocation(mProgramId, "a_position");
  glEnableVertexAttribArray(positionLoc);
  glVertexAttribPointer(positionLoc, 4, GL_FLOAT, GL_FALSE, 0, pos);

  GLint texcoordLoc = glGetAttribLocation(mProgramId, "a_texcoord");
  glEnableVertexAttribArray(texcoordLoc);
  glVertexAttribPointer(texcoordLoc, 2, GL_FLOAT, GL_FALSE, 0, uv);

  GLint texLoc = glGetUniformLocation(mProgramId, "tex_sampler_0");
  glUniform1i(texLoc, 0);

  glActiveTexture(GL_TEXTURE0);
  glBindTexture(mTextureTarget, mTextureId);
  glTexParameteri(mTextureTarget, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(mTextureTarget, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(mTextureTarget, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(mTextureTarget, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

  glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

  glDisableVertexAttribArray(positionLoc);
  glDisableVertexAttribArray(texcoordLoc);
  glBindTexture(mTextureTarget, 0);
}

bool VideoScene::renderVideoFrame()
{
  GstSample *sample = gst_app_sink_pull_sample(GST_APP_SINK(mAppSink));
  if (sample == NULL) {
    return false;
  }

  GstBuffer *buffer = gst_sample_get_buffer(sample);
  GstCaps *caps = gst_sample_get_caps(sample);

  GstVideoInfo info;
  gst_video_info_from_caps(&info, caps);
  gint width = GST_VIDEO_INFO_WIDTH (&info);
  gint height = GST_VIDEO_INFO_HEIGHT (&info);
  if (width != mWidth || height != mHeight) {
    mWidth = width;
    mHeight = height;
    resize();
  }

  GstVideoFrame frame;
  GstMapFlags flags = static_cast<GstMapFlags>(GST_MAP_READ | GST_MAP_GL);
  gst_video_frame_map(&frame, &info, buffer, flags);
  mTextureId = *(GLuint *)frame.data[0];

  if (mContext == NULL) {
    /* Get GStreamer's gl context. */
    gst_gl_query_local_gl_context(mAppSink, GST_PAD_SINK, &mContext);

    /* Check if we have 2D or OES textures */
    GstStructure *s = gst_caps_get_structure(caps, 0);
    const gchar *texture_target_str = gst_structure_get_string(s, "texture-target");
    if (g_str_equal(texture_target_str, GST_GL_TEXTURE_TARGET_EXTERNAL_OES_STR)) {
      mTextureTarget = GL_TEXTURE_EXTERNAL_OES;
    } else if (g_str_equal(texture_target_str, GST_GL_TEXTURE_TARGET_2D_STR)) {
      mTextureTarget = GL_TEXTURE_2D;
    } else {
      g_assert_not_reached();
    }

    /* Initialize shaders */
    createGLTextureAndProgram();
  }

  GstGLSyncMeta *sync_meta = gst_buffer_get_gl_sync_meta(buffer);
  if (sync_meta) {
    /* XXX: the set_sync() seems to be needed for resizing */
    gst_gl_sync_meta_set_sync_point(sync_meta, mContext);
    gst_gl_sync_meta_wait(sync_meta, mContext);
  }

  renderGLTexture();

  gst_video_frame_unmap(&frame);
  gst_sample_unref(sample);

  return true;
}

void VideoScene::threadLoop()
{
  eglBindAPI(EGL_OPENGL_ES_API);
  eglMakeCurrent(mEGLDisplay, mEGLSurface, mEGLSurface, mEGLContext);
  // Work around for deadlocks when video gets out of the user's field of view.
  eglSwapInterval(mEGLDisplay, 0);

  createPipeline();

  while (true) {
    if (!renderVideoFrame()) {
      if (!mRunning)
        break;

      ML_LOG(Info, "Looping");
      gst_element_seek_simple(mPlayBin, GST_FORMAT_TIME, GST_SEEK_FLAG_FLUSH, 0);
      gst_element_set_state(mPlayBin, GST_STATE_PLAYING);
      continue;
    }
    eglSwapBuffers(mEGLDisplay, mEGLSurface);
  }
}

void VideoScene::resetAudioPosition()
{
  if (mAudioNode) {
    auto videoWidth = mQuad->getSize().x;
    mAudioNode->setSpatialSoundPosition (0, {-videoWidth/2, 0.0f, 0.0f});
    mAudioNode->setSpatialSoundPosition (1, {videoWidth/2, 0.0f, 0.0f});
  }
}

static glm::vec2 fitAspectRatio(glm::vec2 destSize, float srcAspect)
{
  float destAspect = destSize.x / destSize.y;
  glm::vec2 size = destSize;
  if (destAspect > srcAspect) {
    size.x = size.y * srcAspect;
  } else {
    size.y = size.x / srcAspect;
  }
  return size;
}

void VideoScene::resize()
{
  float aspectRatio = (float)mWidth * mWidthFactor / mHeight;
  glm::vec2 screenSize = fitAspectRatio(getPrism()->getSize(), aspectRatio);
  mApp.RunOnMainThreadSync([this, screenSize] {
    mQuad->setSize(screenSize);
    resetAudioPosition();
  });
}

void VideoScene::seekTo(float position)
{
  gint64 duration;
  gst_element_query_duration(mPlayBin, GST_FORMAT_TIME, &duration);
  int flags = GST_SEEK_FLAG_FLUSH | GST_SEEK_FLAG_ACCURATE;
  gst_element_seek_simple(mPlayBin, GST_FORMAT_TIME, (GstSeekFlags)flags,
      position * duration);
}
