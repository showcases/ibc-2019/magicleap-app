#! /bin/bash

set -e

if [[ -z $MAGICLEAP_SDK ]]; then
  echo "ERROR: MAGICLEAP_SDK environment variable is not set"
  exit 1
fi

SOURCE_DIR=gst-build
BUILD_DIR=_build
INSTALL_DIR=_install
INSTALL_REAL_DIR=$(realpath $INSTALL_DIR)
HOST=aarch64-linux-android
SYSROOT=$MAGICLEAP_SDK/lumin/usr
PREFIX=/system
LIBDIR=lib64
MABU_TARGET="lumin_debug"
MESON_EXTRA_OPTIONS=""

for i in "$@"; do
  case $i in
    --reconfigure)
      rm -rf $BUILD_DIR
      shift
      ;;
    --release)
      MESON_EXTRA_OPTIONS="$MESON_EXTRA_OPTIONS --buildtype release --strip"
      MABU_TARGET="lumin_release"
      shift
      ;;
  esac
done

# FIXME: Download, build and install GNU libiconv because MLSDK has an old
# version of bionic that does not include iconv.
ICONV_NAME=libiconv-1.16
if [[ ! -d $ICONV_NAME ]]; then
  curl -O -L https://ftp.gnu.org/pub/gnu/libiconv/$ICONV_NAME.tar.gz
  tar xzf $ICONV_NAME.tar.gz
fi

if [[ ! -f  $INSTALL_DIR$PREFIX/$LIBDIR/libiconv.so ]]; then
  mkdir -p $BUILD_DIR/$ICONV_NAME
  pushd $BUILD_DIR/$ICONV_NAME
  env CFLAGS=--sysroot=$SYSROOT \
      CPPFLAGS=--sysroot=$SYSROOT \
      CC=$MAGICLEAP_SDK/tools/toolchains/bin/$HOST-clang \
      AR=$MAGICLEAP_SDK/tools/toolchains/bin/$HOST-ar \
      RANLIB=$MAGICLEAP_SDK/tools/toolchains/bin/$HOST-ranlib \
      ../../$ICONV_NAME/configure --host=$HOST \
                  --with-sysroot=$SYSROOT \
                  --prefix $PREFIX \
                  --libdir $PREFIX/$LIBDIR
  DESTDIR=$INSTALL_REAL_DIR make install
  popd
fi

# Generate cross file by replacing the MLSDK location
cat mlsdk.txt.in | sed s#@MAGICLEAP_SDK@#$MAGICLEAP_SDK# \
                 | sed s#@INSTALL_DIR@#$INSTALL_REAL_DIR# > mlsdk.txt.tmp
rsync --checksum mlsdk.txt.tmp mlsdk.txt
rm mlsdk.txt.tmp

if [[ ! -d $SOURCE_DIR ]]; then
  # Fetch GStreamer 1.16
  git clone https://gitlab.freedesktop.org/gstreamer/gst-build.git --branch 1.16 $SOURCE_DIR

  pushd gst-build
  meson wrap install sqlite
  meson subprojects download gst-plugins-bad libsoup

  # Use custom branch for gst-plugins-bad
  pushd subprojects/gst-plugins-bad
  git remote add xclaesse https://gitlab.freedesktop.org/xclaesse/gst-plugins-bad.git
  git remote update xclaesse
  git checkout xclaesse/magicleap
  popd

  # Use recent libsoup with fixes to be built as subproject
  pushd subprojects/libsoup
  git checkout 2.67.92
  popd

  popd
fi

if [[ ! -f $BUILD_DIR/meson-private/coredata.dat ]]; then
  meson --cross-file mlsdk.txt \
        --prefix $PREFIX \
        --libdir $LIBDIR \
        --libexecdir bin \
        -Db_pie=true \
        -Dcpp_std=c++11 \
        -Dpython=disabled \
        -Dlibav=disabled \
        -Ddevtools=disabled \
        -Dges=disabled \
        -Drtsp_server=enabled \
        -Domx=disabled \
        -Dvaapi=disabled \
        -Dsharp=disabled \
        -Dexamples=disabled \
        -Dgtk_doc=disabled \
        -Dintrospection=disabled \
        -Dnls=disabled \
        -Dbad=enabled \
        -Dgst-plugins-base:gl=enabled \
        -Dgst-plugins-base:gl_api=gles2 \
        -Dgst-plugins-base:gl_platform=egl \
        -Dgst-plugins-base:gl_winsys=android \
        -Dgst-plugins-good:soup=enabled \
        -Dgst-plugins-bad:gl=enabled \
        -Dgst-plugins-bad:magicleap=enabled \
        -Dgst-plugins-bad:dash=enabled \
        -Dgst-plugins-bad:hls=enabled \
        -Dglib:iconv=gnu \
        $MESON_EXTRA_OPTIONS \
        $BUILD_DIR \
        $SOURCE_DIR
fi

ninja -C $BUILD_DIR
DESTDIR=$INSTALL_REAL_DIR meson install -C $BUILD_DIR --only-changed --no-rebuild > /dev/null
mabu -t $MABU_TARGET gstreamer_demo.package
