#include <list>
#include <memory>

#include <ml_logging.h>
#include <lumin/LuminRuntime.h>
#include <lumin/ui/UiKit.h>

#include <gst/rtsp-server/rtsp-server.h>

#include "VideoScene.h"

#define RTSP_PORT "8555"

using namespace lumin;
using namespace lumin::ui;

class GStreamerDemo : public LandscapeApp {
public:
  GStreamerDemo();

private:
  int init() override;
  int deInit() override;
  void onAppStart(const InitArg& initArg) override;
  void onAppPause() override;
  void onAppResume() override;

  static gboolean restartRTSPServer(gpointer user_data);
  void restartRTSPServer();
  void rtspThreadLoop();
  void addVideo(const std::shared_ptr<VideoScene> &videoScene);

  std::list<std::shared_ptr<VideoScene>> mVideos;

  std::thread mRTSPThread;
  GMainContext *mRTSPMainContext;
  GMainLoop *mRTSPMainLoop;
  std::string mRTSPToken;
  GstRTSPServer *mRTSPServer;
};

static void gstreamer_log_cb(GstDebugCategory *category, GstDebugLevel level,
    const gchar *file, const gchar *function, gint line, GObject *object,
    GstDebugMessage *message, gpointer user_data)
{
  ML_LOG(Info, "%s:%d %s() %s", file, line, function,
      gst_debug_message_get(message));
}

static void glib_log_cb(const gchar *log_domain, GLogLevelFlags log_level,
    const gchar *message, gpointer user_data)
{
  ML_LOG(Info, "%s: %s", log_domain, message);
}

GStreamerDemo::GStreamerDemo()
{
}

int GStreamerDemo::init()
{
  auto tmpdir = getTempPath();
  auto bindir = getPackagePath() + "bin";
  auto registry = getWritablePath() + "gstreamer-registry.bin";
  setenv("GIO_MODULE_DIR", bindir.c_str(), 1);
  setenv("GST_PLUGIN_SYSTEM_PATH", bindir.c_str(), 1);
  setenv("GST_REGISTRY", registry.c_str(), 1);
  setenv("XDG_CACHE_HOME", tmpdir.c_str(), 1);
  setenv("GST_DEBUG", "3", 1);
  setenv("G_MESSAGES_DEBUG", "all", 1);

  gst_debug_add_log_function(gstreamer_log_cb, NULL, NULL);
  g_log_set_default_handler(glib_log_cb, NULL);
  gst_init(NULL, NULL);

  mRTSPMainContext = g_main_context_new();
  mRTSPMainLoop = g_main_loop_new(mRTSPMainContext, FALSE);
  mRTSPServer = gst_rtsp_server_new();
  g_object_set(mRTSPServer, "service", RTSP_PORT, NULL);
  mRTSPThread = std::thread(&GStreamerDemo::rtspThreadLoop, this);

  return 0;
}

int GStreamerDemo::deInit()
{
  g_main_loop_quit(mRTSPMainLoop);
  mRTSPThread.join();
  g_main_context_unref(mRTSPMainContext);
  g_main_loop_unref(mRTSPMainLoop);
  g_object_unref(mRTSPServer);

  return 0;
}

void GStreamerDemo::onAppStart(const InitArg& initArg)
{
  mRTSPToken = initArg.getUri();
  g_autoptr(GSource) source = g_idle_source_new();
  g_source_set_callback(source, restartRTSPServer, this, NULL);
  g_source_attach (source, mRTSPMainContext);

  if (mVideos.empty()) {
/*
    addVideo(std::make_shared<VideoScene>(*this,
        "file:///documents/C1/IBC2019-Collabora-Surfing1.mp4", 1280, 720, false));
*/
    addVideo(std::make_shared<VideoScene>(*this,
        "file:///documents/C1/IBC2019-Collabora-3D-Underwater1.mp4", 1280, 720, true));
    addVideo(std::make_shared<VideoScene>(*this,
        "file:///documents/C1/IBC2019-Collabora-audioparticles1-audio.mp4", 1280, 720, false));
    addVideo(std::make_shared<VideoScene>(*this,
        "http://localhost:8080/hls/playlist.m3u8", 1280, 720, false));
  }
}

void GStreamerDemo::onAppPause()
{
  ML_LOG(Info, "pause");
  for (auto &videoScene : mVideos) {
    videoScene->pause();
  }
}

void GStreamerDemo::onAppResume()
{
  ML_LOG(Info, "resume");
  for (auto &videoScene : mVideos) {
    videoScene->resume();
  }
}

void GStreamerDemo::rtspThreadLoop()
{
  g_main_context_push_thread_default(mRTSPMainContext);

  g_autoptr(GSource) source = gst_rtsp_server_create_source(mRTSPServer, NULL, NULL);
  g_source_attach(source, mRTSPMainContext);
  g_main_loop_run(mRTSPMainLoop);
  g_source_destroy(source);

  g_main_context_pop_thread_default(mRTSPMainContext);
}

gboolean GStreamerDemo::restartRTSPServer(gpointer user_data)
{
  auto self = reinterpret_cast<GStreamerDemo *>(user_data);
  self->restartRTSPServer();
  return G_SOURCE_REMOVE;
}

void GStreamerDemo::restartRTSPServer()
{
  if (mRTSPToken.empty())
    return;

  g_autofree gchar *launch = g_strdup_printf("( "
      "rtspsrc latency=50 location=rtsp://127.0.0.1:8554/nosound/mr/720/token=%s ! "
      "rtph264depay ! "
      "rtph264pay pt=96 name=pay0 "
      ")", mRTSPToken.c_str());
  g_autoptr(GstRTSPMountPoints) mounts = gst_rtsp_server_get_mount_points(mRTSPServer);
  auto factory = gst_rtsp_media_factory_new();
  gst_rtsp_media_factory_set_launch(factory, launch);
  gst_rtsp_media_factory_set_shared (factory, TRUE);
  gst_rtsp_mount_points_add_factory(mounts, "/ml1", factory);
}

void GStreamerDemo::addVideo(const std::shared_ptr<VideoScene> &videoScene)
{
  auto volExtents = glm::vec3(2.0f, 2.0f, 0.5f) * 0.5f;
  auto prism = requestNewPrism(volExtents);
  disableContentPersistence(prism);
  setCollisionsEnabled(prism, true);
  prism->setPrismController(videoScene);
  mVideos.push_back(videoScene);
}

int main(int argc, char **argv) {
  GStreamerDemo app;
  int ret = app.run();
  ML_LOG(Info, "quit");
  return ret;
}
